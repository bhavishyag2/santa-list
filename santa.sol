/*
Create a user tracking dapp. Build a system for tracking who is on their nice list and who is on their naughty list. 
Allow for people to pay a fee to get themselves off of the naughty list.
Let users on the nice list earn a portion of the fees based on how many other people are on the nice list.
*/



pragma solidity ^0.8.4;

contract Santa{
    
    address payable[] allUser;
    mapping(address =>address payable[] ) nice;
    mapping(address =>address payable[] ) naughty;
    mapping(address=>mapping(address=>uint)) finall;
    
    //mapping(address=>mapping(address=>uint8)) 
    
    address public owner;
    uint public fees;
    
    constructor(uint _fee) public{
        //allUser.push(msg.sender);
        owner=msg.sender;
        fees=_fee;
    }
    
    
    function addNice(address payable _user) public{
        require(finall[msg.sender][_user]!=2);
        nice[msg.sender].push(_user);
        finall[msg.sender][_user]=1;
        //return true;
    }
    
    
    function addNaughty(address payable _user) public{
        require(finall[msg.sender][_user]!=1);
        naughty[msg.sender].push(_user);
        finall[msg.sender][_user]=2;
        //return true;
    }
    
    function seeAllNice() public view returns(address payable[]){
        return nice[msg.sender];
    }
    
    
    function seeAllNaughty() public view returns(address payable[]){
        address payable[] memory temp=address payable[](nice[msg.sender].length);
        
        address payable[] memory itr=nice[msg.sender];
        
        for(int i=0;i<nice[msg.sender].length;i++){
            if(nice[msg.sender][itr[i]]==2)
                temp.push(itr[i]);
        }
        return temp;
        
        
    }
    
    
    function naughtyToNice(address payable user1) public payable{
        require(finall[msg.sender][user1]==2);
        require(msg.value==fees);
        finall[msg.sender][user1]=1;
        uint len=nice[msg.sender].length;
        
        address payable[] memory itr= nice[msg.sender];
        
        for(uint i=0;i<len;i++){
            nice[msg.sender][itr[i]].transfer(msg.value/len);
        }
        addNice(user1);
        
            
        }
        
        
    }
    
    
    
    
    
    
    
    
    
